﻿namespace Parking
{
    enum VehicleType
    {
        Car = 1,
        Truck = 2,
        Bus = 3,
        Motorcycle = 4
    }
}
