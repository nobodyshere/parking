﻿using System;
using System.IO;
using System.Linq;

namespace Parking
{
    static class Logger
    {
        static public void WriteLog(object o)
        {
            using (StreamWriter file = new StreamWriter(Settings.path, true))
            {
                foreach (var transaction in ParkingStation.transactions.Where(x => x.TransactionTime >= DateTime.Now.AddSeconds(-60)))
                {
                    file.WriteLine(transaction);
                }
            }
        }

        static public string ReadLogs()
        {
            try
            {
                return File.ReadAllText(Settings.path);
            }
            catch (FileNotFoundException)
            {
                return $"File {Settings.path} not found.";
            }
        }
    }
}
