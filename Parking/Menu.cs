﻿using System;

namespace Parking
{
    class Menu
    {
        ParkingStation parking = ParkingStation.GetInstance();
        private bool run = true;
        public Menu()
        {
            ParkingStation.GetInstance();
            Console.SetWindowSize(100, 30);
        }

        public void MainMenu()
        {
            while (run)
            {
                Console.WriteLine("\nPress any key to continue");
                Console.ReadKey();
                Console.Clear();
                var currentColor = Console.ForegroundColor;
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Please choose an option below");
                Console.ForegroundColor = currentColor;

                Console.WriteLine("1. Show vehicles on parking \n" +
                    "2. Add vehicle to parking \n" +
                    "3. Remove vehicle from parking \n" +
                    "4. Show park balance \n" +
                    "5. Fill vehicle balance \n" +
                    "6. Find vehicle by id \n" +
                    "7. Show occupied/free spaces \n" +
                    "8. Show transactions for last minute \n" +
                    "9. Read all logs from Transactions.log \n" +
                    "10. Show profit for last minute. \n" +
                    "0 (Enter). Exit \n");

                GetInput();
            }
        }

        private void GetInput()
        {
            int.TryParse(Console.ReadLine(), out int responce);

            Console.Clear();
            switch (responce)
            {
                case 0:
                    run = false;
                    break;
                case 1:
                    ShowAllVehicle();
                    break;
                case 2:
                    AddVehicle();
                    break;
                case 3:
                    RemoveVehicle();
                    break;
                case 4:
                    GetParkBalance();
                    break;
                case 5:
                    FillVehicleBalance();
                    break;
                case 6:
                    ShowVehicle();
                    break;
                case 7:
                    ShowSpaces();
                    break;
                case 8:
                    ShowTransactionForLastMinute();
                    break;
                case 9:
                    ReadLog();
                    break;
                case 10:
                    ShowProfitForLastMinute();
                    break;
                default:
                    Console.WriteLine($"There are no option like {responce}");
                    GetInput();
                    break;
            }
        }

        private void ShowProfitForLastMinute() => Console.WriteLine($"Total profit per last minute: {parking.GetProfitForLastMinute()}");

        private void ShowTransactionForLastMinute() => parking.GetTransactionsForLastMinute();

        private void ShowAllVehicle() => parking.GetAllVehicle();

        private void AddVehicle()
        {
            Console.WriteLine("Please select vehicle type: \n" +
                "1. Car \n" +
                "2. Truck \n" +
                "3. Bus \n" +
                "4. Motorcycle \n");
            string respond = Console.ReadLine();

            try
            {
                int vehicleType = Convert.ToInt32(respond);
                Console.WriteLine("Please add some balance to this vehicle");
                decimal.TryParse(Console.ReadLine(), out decimal vehicleBalance);

                if (vehicleBalance > 0)
                {
                    var responce = parking.AddVehicle(new Vehicle(vehicleBalance,
                        (VehicleType)vehicleType));

                    if (responce)
                    {
                        Console.WriteLine("Vehicle added");
                    }
                    else
                    {
                        Console.WriteLine("Vehicle not added");
                    }
                }
                else
                {
                    Console.WriteLine("Wrong balance, please try again");
                }
            }
            catch (FormatException)
            {
                Console.WriteLine("Sorry, but vehicle type incorect.");
            }

        }

        private void RemoveVehicle()
        {
            Console.WriteLine("Please enter vehicle id to remove");
            int.TryParse(Console.ReadLine(), out int id);
            if (parking.RemoveVehicle(id))
            {
                Console.WriteLine($"Vehicle id: {id} was removed from parking");
            }
        }

        private void ShowVehicle()
        {
            Console.WriteLine("Please enter vehicle id to find");
            int.TryParse(Console.ReadLine(), out int id);
            if (parking.GetVehicle(id) != null)
            {
                Console.WriteLine(parking.GetVehicle(id));
            }
            else
            {
                Console.WriteLine($"There are no vehicle with id {id}");
            }
        }

        private void GetParkBalance() => Console.WriteLine($"Current balance: {parking.GetBalance()}");

        private void FillVehicleBalance()
        {
            Console.WriteLine("Please enter vehicle id");
            int.TryParse(Console.ReadLine(), out int id);
            Vehicle vehicle = parking.GetVehicle(id);
            try
            {
                Console.WriteLine($"Vehicle id:{id} balance: {vehicle.Balance} fine: {vehicle.Fine}");
                Console.WriteLine("Please enter the amount you want to put on vehicle balance");
                decimal.TryParse(Console.ReadLine(), out decimal vehicleBalance);
                if (vehicleBalance > 0)
                {
                    vehicle.AddBalance(vehicleBalance);
                    Console.WriteLine($"Money added successfull. Current balance: {vehicle.Balance}, Fine: {vehicle.Fine}");
                }
                else
                {
                    Console.WriteLine("Balance can't be zero or negative number, please try again");
                }
            }
            catch (NullReferenceException)
            {
                Console.WriteLine("Cant find vehicle. Please try again");
            }
        }

        private void ShowSpaces()
        {
            if (parking.GetVehiclesCount() == Settings.maxSpace)
            {
                Console.WriteLine($"There are no free space left. All {Settings.maxSpace} slots are occupied.");
            }
            else
            {
                Console.WriteLine($"Empty slots: {Settings.maxSpace - parking.GetVehiclesCount()} \n" +
                    $"Filled slots: {parking.GetVehiclesCount()} \n" +
                    $"Total: {Settings.maxSpace} slots");
            }
        }

        private void ReadLog() => Console.WriteLine(Logger.ReadLogs());
    }
}
