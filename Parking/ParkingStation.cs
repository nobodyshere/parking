﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Parking
{
    class ParkingStation
    {
        private decimal balance;
        private List<Vehicle> transports;
        private Dictionary<Vehicle, Timer> vehicleTimers;
        private Timer logTimer;

        static public List<Transaction> transactions;

        private static readonly Lazy<ParkingStation> instance =
            new Lazy<ParkingStation>(() => new ParkingStation());

        public static ParkingStation GetInstance() => instance.Value;

        public decimal GetProfitForLastMinute() => transactions.Where(x => x.TransactionTime >= DateTime.Now.AddSeconds(-60)).Sum(x => x.Tax);

        private ParkingStation()
        {
            transports = new List<Vehicle>();
            transactions = new List<Transaction>();
            vehicleTimers = new Dictionary<Vehicle, Timer>();
            logTimer = new Timer(Logger.WriteLog, null, Settings.logPeriod, Settings.logPeriod);
        }

        public void GetTransactionsForLastMinute()
        {
            if (transactions.Count > 0)
            {
                foreach (var transaction in transactions.Where(x => x.TransactionTime >= DateTime.Now.AddSeconds(-60)))
                {
                    Console.WriteLine(transaction);
                }
            }
            else
            {
                Console.WriteLine("There are no transactions yet. Please try again later.");
            }
        }

        private void GetPayment(object o)
        {
            var vehicle = o as Vehicle;
            decimal tax = GetTaxRate(vehicle);
            Transaction transaction;
            if (tax > vehicle.Balance)
            {
                vehicle.AddFine(tax * Settings.fine, out transaction);
                transactions.Add(transaction);
            }
            else
            {
                vehicle.Pay(tax, out transaction);
                balance += transaction.Tax;
                transactions.Add(transaction);
            }

        }

        public bool AddVehicle(Vehicle newVehicle)
        {
            if (transports.Count >= Settings.maxSpace)
            {
                Console.WriteLine("There are no space on parking");
                return false;
            }
            else
            {
                Timer timer = new Timer(GetPayment, newVehicle, Settings.period, Settings.period);
                vehicleTimers.Add(newVehicle, timer);
                transports.Add(newVehicle);
                return true;
            }
        }

        public bool RemoveVehicle(int id)
        {
            if (transports.Find(x => x.Id == id) != null)
            {
                transports.RemoveAll(x => x.Id == id);
                return true;
            }
            else
            {
                Console.WriteLine($"There are no transport with id {id}");
                return false;
            }
        }

        public Vehicle GetVehicle(int id) => transports.FirstOrDefault(x => x.Id == id);

        public void GetAllVehicle()
        {
            if (transports.Count > 0)
            {
                foreach (var t in transports)
                    Console.WriteLine(t);
            }
            else
            {
                Console.WriteLine("There are no transport in park");
            }
        }

        public decimal GetBalance() => balance;

        private decimal GetTaxRate(Vehicle vehicle)
        {
            switch (vehicle.Type)
            {
                case VehicleType.Car:
                    return 2m;
                case VehicleType.Truck:
                    return 5m;
                case VehicleType.Bus:
                    return 3.5m;
                case VehicleType.Motorcycle:
                    return 1m;
                default:
                    return 2m;
            }
        }

        public int GetVehiclesCount() => transports.Count;
    }
}
