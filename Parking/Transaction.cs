﻿using System;

namespace Parking
{
    class Transaction
    {
        public DateTime TransactionTime { get; }
        public int TransportId { get; }
        public decimal Tax { get; }
        public decimal Fine { get; }

        public Transaction(int id, decimal tax, decimal fine)
        {
            TransactionTime = DateTime.Now;
            TransportId = id;
            Tax = tax;
            Fine = fine;
        }

        public override string ToString() => $"Transport Id: {TransportId} \t " +
                $"Payed: {Tax} \t " +
                $"Fine: {Fine} \t" +
                $"Time: {TransactionTime.ToLongTimeString()}";

    }
}
