﻿namespace Parking
{
    class Vehicle
    {
        //Not a GUID, because it's not user friendly.
        private static int id = 0;
        public decimal Balance { get; private set; }
        public VehicleType Type { get; private set; }
        public decimal Fine { get; private set; }
        public int Id { get; private set; }

        public Vehicle(decimal balance, VehicleType type)
        {
            Balance = balance;
            Id = ++id;
            Type = type;
        }

        public bool AddBalance(decimal money)
        {
            if (money > 0)
            {
                Balance += money;
                if (Fine > 0 && Fine < Balance)
                {
                    Balance -= Fine;
                    Fine = 0;
                }
                else if (Fine > 0 && Fine > Balance)
                {
                    Fine -= Balance;
                    Balance = 0;
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        public void Pay(decimal tax, out Transaction transaction)
        {
            Balance -= tax;
            transaction = new Transaction(Id, tax, Fine);
        }

        public void AddFine(decimal tax, out Transaction transaction)
        {
            if (Balance > 0)
            {
                tax -= Balance;
                Balance = 0;
            }
            Fine += tax;
            transaction = new Transaction(Id, 0, Fine);
        }

        public override string ToString() => $"Transport id: {Id}, type: {Type.ToString()}, balance: {Balance}, fine: {Fine}";
    }
}
